﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMqTestService1
{
    class Program
    {
        public static void Main()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                //Declare Queues
                channel.QueueDeclare(queue: "rmqexperiments.rpc.fib",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                channel.QueueDeclare(queue: "rmqexperiments.rpc.helloworld",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                channel.BasicQos(0, 1, false);

                var consumerFib = new EventingBasicConsumer(channel);
                consumerFib.Received += (sender, ea) => 
                {
                    NewMessageFib(channel, ea);
                };
                channel.BasicConsume(queue: "rmqexperiments.rpc.fib",
                                             noAck: false,
                                             consumer: consumerFib);
                Console.WriteLine("Started listening on rmqexperiments.rpc.fib");

                var consumerHelloWorld= new EventingBasicConsumer(channel);
                consumerHelloWorld.Received += (sender, ea) =>
                {
                    NewMessageHelloWorld(channel, ea);
                };
                channel.BasicConsume(queue: "rmqexperiments.rpc.helloworld",
                                             noAck: false,
                                             consumer: consumerHelloWorld);
                Console.WriteLine("Started listening on rmqexperiments.rpc.helloworld");


                while (true)
                {
                }
            }
        }

        protected static void NewMessageFib(IModel channel, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var props = ea.BasicProperties;
            var replyProps = channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;
            string response = "";

            try
            {
                var message = Encoding.UTF8.GetString(body);
                int n = int.Parse(message);
                Console.WriteLine(" [.] fib({0})", message);
                response = fib(n).ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(" [.] " + e.Message);
                response = "";
            }
            finally
            {
                var responseBytes = Encoding.UTF8.GetBytes(response);
                channel.BasicPublish(exchange: "",
                                     routingKey: props.ReplyTo,
                                     basicProperties: replyProps,
                                     body: responseBytes);
                channel.BasicAck(deliveryTag: ea.DeliveryTag,
                                 multiple: false);
            }
        }

        protected static void NewMessageHelloWorld(IModel channel, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var props = ea.BasicProperties;
            var replyProps = channel.CreateBasicProperties();
            replyProps.CorrelationId = props.CorrelationId;
            string response = "";

            try
            {
                string message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [.] helloworld request: {0}", message);
                response = "Hello " + message;
            }
            catch (Exception e)
            {
                Console.WriteLine(" [.] " + e.Message);
                response = "";
            }
            finally
            {
                var responseBytes = Encoding.UTF8.GetBytes(response);
                channel.BasicPublish(exchange: "",
                                     routingKey: props.ReplyTo,
                                     basicProperties: replyProps,
                                     body: responseBytes);
                channel.BasicAck(deliveryTag: ea.DeliveryTag,
                                 multiple: false);
            }
        }

        /// <summary>
        /// Assumes only valid positive integer input.
        /// Don't expect this one to work for big numbers,
        /// and it's probably the slowest recursive implementation possible.
        /// </summary>
        private static int fib(int n)
        {
            if (n == 0 || n == 1)
            {
                return n;
            }

            return fib(n - 1) + fib(n - 2);
        }
    }
}
