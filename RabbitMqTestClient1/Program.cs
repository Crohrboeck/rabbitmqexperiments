﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMqTestClient1
{
    class RPCClient
    {
        private IConnection connection;
        private IModel channel;
        private string replyQueueName;
        private QueueingBasicConsumer consumer;

        public RPCClient()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            replyQueueName = channel.QueueDeclare().QueueName;
            consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(queue: replyQueueName,
                                 noAck: true,
                                 consumer: consumer);
        }

        public string Call(string message, string queueName)
        {
            var corrId = Guid.NewGuid().ToString();
            var props = channel.CreateBasicProperties();
            props.ReplyTo = replyQueueName;
            props.CorrelationId = corrId;

            var messageBytes = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: "",
                                 routingKey: queueName,
                                 basicProperties: props,
                                 body: messageBytes);

            while (true)
            {
                var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                if (ea.BasicProperties.CorrelationId == corrId)
                {
                    return Encoding.UTF8.GetString(ea.Body);
                }
            }
        }

        public void Close()
        {
            connection.Close();
        }
    }

    class Program
    {
        public static void Main()
        {
            var rpcClient = new RPCClient();

            while (true)
            {
                Console.WriteLine(" [x] Requesting fib(30)");
                var response = rpcClient.Call("30", "rmqexperiments.rpc.fib");
                Console.WriteLine(" [.] Got '{0}'", response);

                Console.WriteLine(" [x] Requesting fib(10)");
                response = rpcClient.Call("10", "rmqexperiments.rpc.fib");
                Console.WriteLine(" [.] Got '{0}'", response);

                Console.WriteLine(" [x] Requesting helloworld for Testi Tester");
                response = rpcClient.Call("Testi Tester", "rmqexperiments.rpc.helloworld");
                Console.WriteLine(" [.] Got '{0}'", response);

                Console.WriteLine(" [x] Requesting helloworld for Harald Tester");
                response = rpcClient.Call("Harald Tester", "rmqexperiments.rpc.helloworld");
                Console.WriteLine(" [.] Got '{0}'", response);
            }

            rpcClient.Close();
        }
    }
}
